val kotlinVersion          = "1.8.10"
val ktorVersion            = "2.3.2"
val koinVersion            = "3.3.1"
val agObservabilityVersion = "0.4.7"
val agKtorVersion          = "0.1.2"

val kotlinDeps = Seq(
  "org.jetbrains.kotlin" % "kotlin-reflect"       % kotlinVersion,
  "org.jetbrains.kotlin" % "kotlin-serialization" % kotlinVersion,
  // Ktor
  "com.agoda.commons" % "ag-http-server-ktor"                 % agKtorVersion,
  "io.ktor"           % "ktor-client-core-jvm"                % ktorVersion,
  "io.ktor"           % "ktor-client-okhttp-jvm"              % ktorVersion,
  "io.ktor"           % "ktor-client-content-negotiation-jvm" % ktorVersion,
  "io.ktor"           % "ktor-serialization-kotlinx-json-jvm" % ktorVersion
)
val observabilityDeps = Seq(
  // Opentelmetry
  "io.opentelemetry.instrumentation" % "opentelemetry-jdbc"              % "1.24.0-alpha",
  "com.agoda.commons"                % "ag-observability-sdk"            % agObservabilityVersion,
  "com.agoda.commons"                %% "ag-observability-tracing-scala" % agObservabilityVersion,
  "com.agoda.commons"                % "ag-observability-logging-adp"    % agObservabilityVersion,
  // Logging
  "org.slf4j"      % "jul-to-slf4j"    % "2.0.7",
  "ch.qos.logback" % "logback-classic" % "1.3.6",
  "ch.qos.logback" % "logback-core"    % "1.3.6"
)
val testDeps = Seq(
  "io.ktor"              % "ktor-server-tests-jvm" % ktorVersion   % Test,
  "io.ktor"              % "ktor-server-test-host" % ktorVersion   % Test,
  "org.jetbrains.kotlin" % "kotlin-test"           % kotlinVersion % Test,
  "org.mockito.kotlin"   % "mockito-kotlin"        % "4.1.0"       % Test,
  "org.junit.jupiter"    % "junit-jupiter-api"     % "5.8.1"       % Test,
  "io.cucumber"          % "cucumber-java"         % "4.7.0"       % Test,
  "io.cucumber"          % "cucumber-junit"        % "4.7.0"       % Test
)
val koinDeps = Seq(
  "io.insert-koin" % "koin-ktor"         % koinVersion,
  "io.insert-koin" % "koin-logger-slf4j" % koinVersion,
  "io.insert-koin" % "koin-test"         % "3.3.3" % Test
)
libraryDependencies ++= kotlinDeps ++ observabilityDeps ++ koinDeps ++ testDeps ++ Seq(
  "com.agoda.commons"            % "ag-vault-kotlin"         % "1.1.0",
  "com.fasterxml.jackson.module" %% "jackson-module-scala"   % "2.14.2",
  "net.aichler"                  % "jupiter-interface"       % JupiterKeys.jupiterVersion.value % Test,
  "com.agoda.commons"            % "ag-dynamic-state-consul" % "3.0.1"
)
